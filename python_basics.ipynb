{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# Welcome to Python basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Welcome to this very brief introduction to Python basics prepared for M-SEA 2022. It is adapted from a course titled *Geospatial Technologies Computational Toolkit* and incorporates contributions from students and colleagues at University of Arkansas. While this introduction is tailored to some extent based on interest in geographic information science (GIScience) or geospatial data science, the skills explored can be adapted to a wide variety of Python applications. No prior programming experience is required. To function as intended, this Jupyter Notebook document should be uploaded to Google Colab using a Google account that has been granted access to Google Earth Engine. I will provide a demonstration of how to work with this notebook, and attempt to answer any questions that may arise along the way.\n",
    "\n",
    "## Instructor\n",
    "\n",
    "Jason A. Tullis, Professor of Geography<br>\n",
    "Department of Geosciences and Center for<br>\n",
    "Advanced Spatial Technologies, 321 JBHT<br>\n",
    "Fulbright College of Arts and Sciences<br>\n",
    "University of Arkansas, Fayetteville, AR<br>\n",
    "Phone 479.575.8784 Email jatullis@uark.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# Key elements of a Python program"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true,
    "hidden": true
   },
   "source": [
    "## Names\n",
    "\n",
    "Names are given to **variables** (e.g., celsius), **modules**, or **functions** (e.g., conversion).\n",
    "\n",
    "Names are called **identifiers**.\n",
    "\n",
    "Every identifier **must** begin with a letter or the underscore character (\"_\") followed by any sequence of letters, digits, or underscores.\n",
    "\n",
    "Identifiers are **case sensitive**.\n",
    "\n",
    "## Valid examples of names\n",
    "\n",
    "X, which is **different** from x\n",
    "\n",
    "Celsius\n",
    "\n",
    "Spam\n",
    "\n",
    "SpAm\n",
    "\n",
    "Spam_and_Eggs1\n",
    "\n",
    "Spam1_And_Eggs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Invalid variable names\n",
    "\n",
    "Since variable names can consist of letters, digits, and the underscore, a varaiable name **cannot**:\n",
    "    \n",
    "    a) Begin with a digit (e.g., 1point)\n",
    "    \n",
    "    b) Contain mathematical operators (e.g., +point, *stars)\n",
    "    \n",
    "    c) Contain spaces (e.g., John Doe, space bar)\n",
    " \n",
    "A variable name should **ideally make sense**, except in the case of some local well known variable (e.g., e is short for Exception).\n",
    " \n",
    "For longer variable names, one possibility is to mix lower_case_with_underscore (since spaces are not allowed). Another option is to StartEachWordWithUppercase. **However, it is advisable to keep variable names clear and easy to read!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 1\n",
    "In the cell below, write **degrees Celsius = 22**, then select Ctrl + Enter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Why did you receive a syntax error?\n",
    "\n",
    "From what you recently learned about variable names, provide a valid variable name and re-run the code."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## What are variables?\n",
    "\n",
    "Essentially, variables are like boxes where you can store values (e.g., x = 2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = 2\n",
    "x + x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "When we assign the variable x with the value of 2, the output value is 4.\n",
    "\n",
    "When a variable changes, the old value is **substituted** by the new one.\n",
    "\n",
    "Variables can be reassigned as many times as you want."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 2\n",
    "In the cell below, myVariable has a value of 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "myVariable = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "1. Run the cell above and then the cell below (Ctrl + Enter for each); did you get an output of 0?\n",
    "\n",
    "2. In the same cell, give myVariable another value, and below that write myVariable again.\n",
    "\n",
    "3. Run the line for myVariable below; what did you get? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "myVariable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Reserved words\n",
    "\n",
    "Some identifiers are part of Python itself. These identifiers are known as **reserved words**, meaning they are not available for us to use them as names for variables in our programs.\n",
    "\n",
    "Some of these reserved words are: **and, del, for, is, raise, assert, elif, in**.\n",
    "    \n",
    "For a complete list, you can check Python Software Foundation's (2019) \"Lexical Analysis: Keywords\" documentation in our class library.\n",
    "\n",
    "## Expressions\n",
    "\n",
    "The fragment of code that can produce or calculate new data values are called **expressions**:\n",
    "   \n",
    "     x = 2 + 2\n",
    "     2 + 2 is the expression\n",
    "     \n",
    "**Literals** are used to represent specific values (e.g., 1, 2.0, 100).\n",
    "   \n",
    "Expressions can be combined using operators: +, -, *, /, ^, etc.\n",
    "   \n",
    "The normal mathematical precedence applies.\n",
    "\n",
    "## Output statements\n",
    "\n",
    "A print statememt can print any number of expressions.\n",
    "   \n",
    "Successive print statements will display on separate lines.\n",
    "   \n",
    "A bare print will print a blank line.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "For example, **print ('')** is used for strings (characters placed within the single or double quotes) and **print ( )** is used for expressions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Geographic information systems (GIS)\n"
     ]
    }
   ],
   "source": [
    "print('Geographic information systems (GIS)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4\n"
     ]
    }
   ],
   "source": [
    "print(2+2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 3\n",
    "\n",
    "The print command below is for a string; read the error for the print statement and correct it based on what you have learned so far."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "invalid syntax (<ipython-input-1-8d1861339975>, line 1)",
     "output_type": "error",
     "traceback": [
      "\u001b[1;36m  File \u001b[1;32m\"<ipython-input-1-8d1861339975>\"\u001b[1;36m, line \u001b[1;32m1\u001b[0m\n\u001b[1;33m    print(I am learning a lot about Python!)\u001b[0m\n\u001b[1;37m             ^\u001b[0m\n\u001b[1;31mSyntaxError\u001b[0m\u001b[1;31m:\u001b[0m invalid syntax\n"
     ]
    }
   ],
   "source": [
    "print(I am learning a lot about Python!)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Assignment statements\n",
    "\n",
    "In Python, the equal sign = is referred to as an assignment operator.\n",
    "\n",
    "The expression on the right-hand side of = produces a value associated with the variable on the left-hand side of =: \n",
    "\n",
    "     <variable> = <expression>\n",
    "     \n",
    "Several values can be respectively calculated at the same time with the following syntax:\n",
    "     \n",
    "     <variable>,<variable>...=<expression>,<expression>,..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 4\n",
    "Change the expressions to get new values for x and y."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4 10\n"
     ]
    }
   ],
   "source": [
    "x, y = 2+2, 5+5\n",
    "print(x,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Another advantage of this syntax is to swap the value of variables, as shown for x and y below.\n",
    "\n",
    "How could this be useful for writing a large script?\n",
    "\n",
    "Try changing the values below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "47 750\n",
      "750 47\n"
     ]
    }
   ],
   "source": [
    "x = 47\n",
    "y = 750\n",
    "print(x,y)\n",
    "\n",
    "x,y = y,x\n",
    "\n",
    "print(x,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Congratulations!\n",
    "\n",
    "Working with Python (or any language for that matter) takes practice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# Stages of Python program development"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The process of creating a python program is often broken down into stages according to the information that is produced in each phase:\n",
    "\n",
    "1. Analyze the problem\n",
    "2. Determine the specifications\n",
    "3. Create a design\n",
    "4. Implement the design\n",
    "5. Test/debug the program\n",
    "6. Maintain the program"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Example of temperature conversion\n",
    "\n",
    "**Analysis**: the temperature is given in Celsius, and the user wants it expressed in Fahrenheit.\n",
    "\n",
    "**Specification**:\n",
    "\n",
    "    Input: temperature in Celsius\n",
    "    Output: temperature in Fahrenheit\n",
    "    Output = 9/5(input)+32\n",
    "\n",
    "**Design**:\n",
    "    \n",
    "    Prompt the user for input (Celsius temperature)\n",
    "    Process it to convert it to Fahrenheit using F=C*9/5+32   \n",
    "    Output the result by displaying it on the screen\n",
    "\n",
    "## Pseudocode versus Python code\n",
    "\n",
    "Before you start coding, it is sometimes a good idea to write a rough draft of the program in **pseudocode**.\n",
    "\n",
    "Pseudocode is written in English (not a programming language), and it describes what a program does step-by-step.\n",
    "\n",
    "When using pseudocode, we can concentrate on the algorithm rather than the programming language or its particular syntax.\n",
    "\n",
    "Here is one possible way to write the pseudocode for the above temperature conversion example:\n",
    "    \n",
    "    Input the temperature in degrees Celsius (call it celsius)\n",
    "    Calculate fahreneit as celsius*9/5+32\n",
    "    Output fahrenheit\n",
    "    Display results on screen\n",
    "\n",
    "Now this can be converted into Python; here is one possible example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What is the temperature in Celsius? 100\n",
      "The temperature is 212.0 degrees Fahrenheit\n"
     ]
    }
   ],
   "source": [
    "celsius = int(input('What is the temperature in Celsius? '))\n",
    "fahrenheit = celsius*9/5+32\n",
    "print('The temperature is '+str(fahrenheit)+' degrees Fahrenheit')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 1\n",
    "\n",
    "From what you've learned so far, try writing a script to convert Fahrenheit to Celsius.\n",
    "\n",
    "**Analysis**: the temperature is given in Fahrenheit, and the user wants to express it in Celsius.\n",
    "\n",
    "**Specification**:\n",
    "\n",
    "    Input: temperature in Fahrenheit\n",
    "    Output: temperature in Celsius\n",
    "    Output: (input-32)x5/9\n",
    "\n",
    "**Design**:\n",
    "\n",
    "    Prompt the user for input (Celsius temperature)\n",
    "    Process it to convert it to Fahrenheit using F=C*9/5+32\n",
    "    Output the result by displaying it on the screen\n",
    "\n",
    "Write your script in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Functions\n",
    "\n",
    "If we want to do temperature conversions multiple times, then we can develop a **function** that can be called repeatedly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What is the temperature in Celsius? 100\n",
      "The temperature is 212.0 degrees Fahrenheit\n"
     ]
    }
   ],
   "source": [
    "def conversion():\n",
    "    celsius = int(input('What is the temperature in Celsius? '))\n",
    "    fahrenheit = celsius*9/5+32\n",
    "    print('The temperature is '+str(fahrenheit)+' degrees Fahrenheit')\n",
    "conversion()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Add your own notes\n",
    "\n",
    "When working in Jupyter Notebook, you may want to add your own notes or to modify the file in any way that helps you best learn the concepts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## A Python function\n",
    "\n",
    "Usually someone would want to execute several statements together that solve a common problem.\n",
    "\n",
    "One way to do this is to use a **function**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def hello():\n",
    "    print('Hello world of GIScience!')\n",
    "    print('Python functions are extremely useful.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The first line tells Python you are **defining** a new function called hello.\n",
    "\n",
    "The next two lines are **indented** to show that they are part of the hello function.\n",
    "\n",
    "If we were to hit enter and dedent, any subsequent code entered at that point would no longer be part of this function.\n",
    "\n",
    "## Invoking a function\n",
    "\n",
    "Notice that nothing has happened yet!\n",
    "\n",
    "You’ve defined the function, but you haven’t told Python to perform the function.\n",
    "\n",
    "A function is **invoked** by typing its name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello world of GIScience!\n",
      "Python functions are extremely useful.\n"
     ]
    }
   ],
   "source": [
    "hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The parentheses () are very important components; a function can have inputs called **parameters** that are placed between the ()s and can be different every time you call the function.\n",
    "\n",
    "When you use parameters, you can customize the output of your function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "def greet(person):\n",
    "    print('Hello', person)\n",
    "    print('Good day.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 1\n",
    "\n",
    "Try invoking the greet function based on what you've learned so far!\n",
    "\n",
    "Add your name in single or double quotation marks as the parameter (e.g., ('Jean'))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# Anatomy of a Python program"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "When you exit the Python interpreter (e.g., IDLE), the functions you have written cease to exist.\n",
    "\n",
    "Programs are usually composed of functions, **modules**, or **scripts** that are saved on disk so that they can be used again and again.\n",
    "\n",
    "A **module file** is a text file so technically it could be created in the most basic text editing software (e.g., Notepad) that contains function definitions.\n",
    "\n",
    "A **programming environment** (e.g., IDLE, PyCharm, Visual Studio Code, etc.) is designed to help programmers write programs and usually includes automatic **indenting**, highlighting, etc.\n",
    "\n",
    "If a programmer asks \"What is your favorite text editor?\", they are likely referring to the more sophisticated programming environments; at the end of the day, we are just editing text documents (as opposed to binary files such as Microsoft Word documents) when we write Python scripts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What is the temperature in Celsius? 100\n",
      "The temperature is 212.0 degrees Fahrenheit\n"
     ]
    }
   ],
   "source": [
    "# A program(conversion.py) to convert Celsius to Fahrenheit\n",
    "\n",
    "def conversion():\n",
    "    celsius = int(input('What is the temperature in Celsius? '))\n",
    "    fahrenheit = celsius*9/5+32\n",
    "    print('The temperature is '+str(fahrenheit)+' degrees Fahrenheit')\n",
    "conversion()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The **#** is to indicate a single line comment or note to yourself (or to end users) that provides some metadata for your program; comments (#) are ignored by the interpreter.\n",
    "\n",
    "The name **<filename>.py** (replace within the <> with what you really want to call it) will be used to save your work and indicate to the operating system that the text file is intended to be a Python program.\n",
    "\n",
    "In this code we are defining a new function called **conversion**.\n",
    "\n",
    "The **conversion()** at the end tells Python to run or execute the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Syntax\n",
    "\n",
    "Python is **case sensitive** on ALL names including the names of classes, objects, variables, functions, etc.\n",
    "\n",
    "**Indentation** is extremely important in Python for grouping statements at different levels.\n",
    "\n",
    "    4 spaces or columns are usually used to indent each level of statement\n",
    "\n",
    "Indentation can be done by using the **space** key or the **tab** key.\n",
    "\n",
    "Tabs in a Python text editor normally represent 4 spaces, but this can be altered (e.g., Settings in IDLE).\n",
    "\n",
    "Indentation must be **consistent** within the entire program.\n",
    "    \n",
    "You generally cannot safely mix space and tabs in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "## Exercise 2\n",
    "As needed, try correcting the scripts below based on what you've learned so far!\n",
    "\n",
    "Read the errors you receive to guide you on problem solving.\n",
    "\n",
    "As always in Jupyter Notebook, select Ctrl+Enter to run or rerun a cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "ename": "IndentationError",
     "evalue": "unexpected indent (<ipython-input-5-5b0c9ae784ab>, line 2)",
     "output_type": "error",
     "traceback": [
      "\u001b[1;36m  File \u001b[1;32m\"<ipython-input-5-5b0c9ae784ab>\"\u001b[1;36m, line \u001b[1;32m2\u001b[0m\n\u001b[1;33m    MyLiSt[0]\u001b[0m\n\u001b[1;37m    ^\u001b[0m\n\u001b[1;31mIndentationError\u001b[0m\u001b[1;31m:\u001b[0m unexpected indent\n"
     ]
    }
   ],
   "source": [
    "myList = [1,2,3,4]\n",
    "    MyLiSt[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "unexpected EOF while parsing (<ipython-input-6-177713e24b65>, line 1)",
     "output_type": "error",
     "traceback": [
      "\u001b[1;36m  File \u001b[1;32m\"<ipython-input-6-177713e24b65>\"\u001b[1;36m, line \u001b[1;32m1\u001b[0m\n\u001b[1;33m    def conversion():\u001b[0m\n\u001b[1;37m                     ^\u001b[0m\n\u001b[1;31mSyntaxError\u001b[0m\u001b[1;31m:\u001b[0m unexpected EOF while parsing\n"
     ]
    }
   ],
   "source": [
    "def conversion():"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "This line is the beginning of the definition of a function called conversion.\n",
    "\n",
    "Since the program is simple and has only one module, it could easily have been written without the conversion function.\n",
    "\n",
    "As we will discuss, functions in one module are reusable in other modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'fahrenheit' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-7-fcd38130ee78>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m'The temperature is '\u001b[0m\u001b[1;33m+\u001b[0m\u001b[0mstr\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mfahrenheit\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m+\u001b[0m\u001b[1;34m' degrees Fahrenheit'\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m: name 'fahrenheit' is not defined"
     ]
    }
   ],
   "source": [
    "print('The temperature is '+str(fahrenheit)+' degrees Fahrenheit')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "This line causes Python to **print** a message with the result of our program.\n",
    "\n",
    "Typing **str()** calls a built-in function of Python that converts a number data type to a string data type.\n",
    "\n",
    "The content of the string is defined by double or single quotes in a consistent format.\n",
    "\n",
    "Strings can be concatenated by using '+' as many times as needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "What is the temperature in Celsius? 234\n"
     ]
    }
   ],
   "source": [
    "celsius = int(input('What is the temperature in Celsius? '))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The word celsius above is an example of a **variable**.\n",
    "\n",
    "A variable is used to assign a name to a value so that we can refer to it later.\n",
    "\n",
    "The quoted information is displayed, and the number typed by the user in response is stored in the variable **celsius**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "fahrenheit = celsius*9/5+32"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "This is called an **assignment statement**.\n",
    "\n",
    "The part on the right-hand side of the '=' is a mathematical expression.\n",
    "\n",
    "The * is an **operator** used to indicate multiplication.\n",
    "\n",
    "Once the value of the expression is computed, it is stored back into (assigned to) the fahrenheit variable on the left of the '=' character.\n",
    "\n",
    "A **statement** is composed of **variables** and **operators**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "# Script converts Celsius to Fahrenheit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Again, all the lines that start with a # symbol are called comments.\n",
    "\n",
    "They are intended for human interpretation and are ignored by Python.\n",
    "\n",
    "Python skips all of the the text from the # to the end of that line.\n",
    "\n",
    "They are useful to present the purpose of each program, and to explain more difficult parts of the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# Using the math library"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Besides arithmetic calculations using operators (e.g., +,-,$*$,/,//,$**$,%,abs), there are a lot of other math functions available through the **math library**.\n",
    "\n",
    "A Python **library** is essentially a module which contains some very useful definitions and functions."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Suppose you want to write a program which computes the roots of a quadratic equation:\n",
    "\n",
    "x = -b±√(b²-4ac)/2a\n",
    "\n",
    "You have seen this equation before, but this time we will focus on how the math library allows us to calculate the square root. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "To use the math library, you will need to make sure that the following line is included in the program: **import math**.\n",
    "\n",
    "Importing a library makes whatever functions defined within it available to the program.\n",
    "\n",
    "To access the square root library routine (sqrt), we need to access it as math.sqrt(x).\n",
    "\n",
    "Using the dot notation, Python knows to use the sqrt function found in the math library module.\n",
    "\n",
    "## Exercise 1\n",
    "\n",
    "The function below calculates the roots in a quadratic equation. \n",
    "\n",
    "Add comments to each line to describe the process of script. \n",
    "\n",
    "Edit any errors so that the script can run properly. \n",
    "\n",
    "Make sure to invoke the function for the output. \n",
    "\n",
    "Use coefficents (1, 9, 18) when testing the function.\n",
    "\n",
    "Note that **this program could potentially produce errors** when there are no real solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "hidden": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This program finds the solutions to a quadratic.\n",
      "Please enter coefficent a: 1\n",
      "Please enter coefficent b: 9\n",
      "Please enter coefficent c: 18\n",
      "The solutions are: -6.0 and -3.0\n"
     ]
    }
   ],
   "source": [
    "import math\n",
    "def quadratic():\n",
    "    print('This program finds the solutions to a quadratic.')\n",
    "    a = int(input('Please enter coefficent a: '))\n",
    "    b = int(input('Please enter coefficent b: '))\n",
    "    c = int(input('Please enter coefficent c: '))\n",
    "    calRoot = math.sqrt((b*b)-4*a*c)\n",
    "    root1 = (-b -calRoot)/2*a\n",
    "    root2 = (-b +calRoot)/2*a\n",
    "    print('The solutions are: '+str(root1)+' and '+str(root2))\n",
    "quadratic()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "For more information on the math library, refer to the [corresponding math documentation](https://docs.python.org/3.6/library/math.html?highlight=math#module-math) (Python Software Foundation 2019)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
